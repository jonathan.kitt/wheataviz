# testing wheat_scatter function

# Import test data ----

d1 <- vroom::vroom("misc/data_test.txt")

# Generate ideogram ----

genome.version <- "IWGSCv1"
chrom <- unique(d1$chromosome)

i.data <- wheat_chromosomes |>
  filter(genome_version == genome.version, chromosome == chrom)

(ideo <- ggplot() +
  geom_rect(data = i.data,
            aes(xmin = region_min, xmax = region_max,
                ymin = 0, ymax = 1,
                fill = region_name),
            show.legend = FALSE) +
  geom_text(data = i.data,
            aes(x = region_min + ((region_max - region_min) / 2),
                y = 0.5,
                label = region_name)) +
  labs(x = "Mb",
       y = unique(i.data$chromosome),
       caption = genome.version) +
  scale_fill_manual(values = c("R1" = "#2e3c9d",
                               "R2a" = "#41b7c5",
                               "C" = "#ffffcd",
                               "R2b" = "#41b7c5",
                               "R3" = "#2e3c9d")) +
  scale_x_continuous(limits = c(0, max(i.data$region_max)),
                     breaks = seq(100e6, round(max(i.data$region_max)), 100e6),
                     labels = paste0(seq(100, round(max(i.data$region_max) / 1e6), 100), " Mb"),
                     expand = c(0, 0)) +
  theme(panel.background = element_blank(),
        axis.title.x = element_blank(),
        axis.title.y = element_text(angle = 0, vjust = 0.5),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank())
)

# Combine plots ----

(p <- ggplot(data = d1,
             mapping = aes(x = position, y = lrn)) +
   geom_point(size = 0.5) +
   scale_x_continuous(limits = c(0, max(i.data$region_max)),
                      breaks = seq(100e6, round(max(i.data$region_max)), 100e6),
                      labels = paste0(seq(100, round(max(i.data$region_max) / 1e6), 100), " Mb"),
                      expand = c(0, 0)) +
   labs(x = "") +
   theme(plot.margin = margin(b = -20),
         axis.text.x = element_blank(),
         axis.ticks.x = element_blank())
)

p1 <- p / ideo +
  plot_layout(heights = c(9, 1))

p1 + p1 +
  plot_layout(ncol = 2)
